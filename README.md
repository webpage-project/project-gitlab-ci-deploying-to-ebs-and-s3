# Web Application CI/CD Pipeline

This project demonstrates a complete CI/CD pipeline for a web application using GitLab CI/CD. The pipeline includes stages for building, testing, packaging, and deploying the application to staging and production environments.

## Pipeline Stages

The CI/CD pipeline consists of the following stages:

1. **Build**: Builds the web application using Node.js and generates a production-ready build artifact.
2. **Test**: Runs unit tests and lints the codebase to ensure code quality and prevent regressions.
3. **Package**: Builds a Docker image of the web application and pushes it to the GitLab Container Registry.
4. **Deploy Staging**: Deploys the web application to a staging environment on AWS S3.
5. **Deploy Production**: Deploys the web application to a production environment on AWS Elastic Beanstalk.

## Deployment Stages

The pipeline includes two separate deployment stages:

1. **Deploy to AWS S3**:
   - This stage is triggered automatically when changes are pushed to the default branch.
   - The web application artifacts are synced with an AWS S3 bucket, making the application accessible via the S3 static website hosting feature.
   - This stage is suitable for staging or testing environments.

2. **Deploy to AWS Elastic Beanstalk**:
   - This stage is triggered manually and requires explicit approval.
   - The web application is packaged into a Docker container and deployed to an AWS Elastic Beanstalk environment.
   - Elastic Beanstalk provides a scalable and managed platform for running web applications in production.
   - The deployment is performed using the AWS CLI and the Elastic Beanstalk API.

## Prerequisites

Before running the pipeline, make sure you have the following:

- GitLab account with CI/CD enabled
- Node.js and Yarn installed on your local machine
- Docker installed on your local machine
- AWS account with necessary credentials and permissions

## Configuration

1. Set up the following environment variables in your GitLab project settings:
   - `AWS_ACCESS_KEY_ID`: Your AWS access key ID.
   - `AWS_SECRET_ACCESS_KEY`: Your AWS secret access key.
   - `AWS_S3_BUCKET`: The name of the S3 bucket for deploying the web application.
   - `CI_REGISTRY_USER`: Your GitLab registry username.
   - `CI_REGISTRY_PASSWORD`: Your GitLab registry password.
   - `GITLAB_DEPLOY_TOKEN`: A deploy token for accessing the GitLab registry.

2. Update the `APP_NAME` and `APP_ENV_NAME` variables in the `.gitlab-ci.yml` file to match your AWS Elastic Beanstalk application and environment names.

3. Customize the `Dockerrun.aws.json` and `auth.json` templates in the `templates` directory according to your application requirements.

## Usage

1. Push your code changes to the GitLab repository.
2. The CI/CD pipeline will automatically trigger based on the defined stages and rules.
3. Monitor the pipeline execution in the GitLab CI/CD interface.
4. Once the pipeline completes successfully, your web application will be deployed to the specified environments.

## Additional Notes

- The pipeline uses Docker to build and package the web application into a container image.
- The container image is pushed to the GitLab Container Registry for easy distribution and deployment.
- AWS S3 is used for hosting the staging environment, while AWS Elastic Beanstalk is used for the production environment.
- The pipeline includes steps to verify the successful deployment by making HTTP requests to the deployed application.

For more information and customization options, refer to the GitLab CI/CD documentation and the AWS documentation for S3 and Elastic Beanstalk.